#########################################
#       CHATBOT ENGINE IN RENPY         #
#########################################
#            By 3nigmatico              #
#                                       #
#    This is an implementation of my    #
#  chatbot engine in Python used in a   #
#   RenPy game for a more interactive   #
#              experience.              #
#                                       #
#########################################


#!/usr/bin/python3
import xml.etree.ElementTree as ET
import random # Random functions
import re #reeeeeeeeeeeee
import time
import json #Base
import os

# Chatbot engine version
med_ver = "0.70b 7E2-06"

# Regexes to identify cerain parameters in the patterns.
# These will be replaced with different variable values.
rep_params = {
    "match_param":re.compile("(\[:([0-9]+):]+)"),
    "match_bot_param":re.compile("\$\[([A-Za-z0-9]+)\]"),
    "setvar": re.compile(r"@\[\s([a-zA-Z0-9\s,\.]+)=([a-zA-Z0-9\s,\.]+)\s\]"),
    "getvar": re.compile(r"@\[\s([a-zA-Z0-9\s,\.]+)\s\]")
}

# Weights that dictate the value of specific special characters
weights = {
        "*": 2,
        "_": 4,
        "?": 6,
        "^": 1
}

# Main class. This is what you should instantiate in your game.
class BotClient():
    def __init__(self, config):
        print("Medea Engine ver %s" % med_ver)
        self.seeddate = time.localtime(time.time())
        random.seed(self.seeddate.tm_sec * self.seeddate.tm_min * self.seeddate.tm_yday + self.seeddate.tm_mon - self.seeddate.tm_year)
        self.dictionary = Dictionary()
        rawdata = config.read()
        self.bot = json.loads(rawdata)
        config.close()
    def loadDict(self, fhand):
        self.tree = ET.parse(fhand)
        self.root = self.tree.getroot().find("Concept")
        for pattern in self.root.findall('Default'):
            self.dictionary.make_pattern(pattern, True)
        for pattern in self.root.findall('Model'):
            self.dictionary.make_pattern(pattern, False)
        fhand.close()
    def chat(self, msg):
        chat_lines = re.split(r'[\.\?!]',msg)
        chat_lines = [x.upper().strip() for x in chat_lines if x]
        respl = ""
        for cl in chat_lines:
            respl = respl + self.dictionary.process_response(cl, self.bot) + ". "
        return respl
    def getbotdata(self, param):
        return self.bot[param]

# This class contains all the patterns, and the functions to interact with them, including
# searches.
class Dictionary():
    def __init__(self):
        self.rules = 0
        self.words = {"*":[],"_":[],"?":[],"^":[]}
        self.previous = ""
        self.default_pattern = None
        self.vars = {}
    def make_pattern(self, model, default=False):
        ifconditions = []
        replies = ()
        if model.find('If') == None:
            if model.find('Random') == None:
                replies = tuple(model.findall('Response'))
            else:
                replies = tuple(model.find('Random').findall("Item"))
        else:
            # print("-- Conditional rule")
            conditions = model.findall('If')
            for cond in conditions:
                print("Cond ", cond)
                prev = cond.get('previous')
                cond_replies = cond.findall('Response') + cond.findall('Item')
                ifconditions.insert(len(ifconditions), {"type":"Response","value":prev,"inverse":False,"Replies":cond_replies})
        inp = model.find('Pattern').text
        cpat = ChatPattern(model.find('Pattern').text, replies, ifconditions if len(ifconditions)>0 else None)
        if not default:
            self.insert_word(inp.lstrip().split(" ")[0], cpat)
        else:
            self.default_pattern = cpat
    def insert_word(self, word, pattern):
        if(self.search_word(word) == False):
            self.words[word] = []
            self.words[word].insert(len(self.words[word]), pattern)
            self.rules = self.rules + 1
        else:
            self.words[word].insert(len(self.words[word]), pattern)
            self.rules = self.rules + 1
    def search_word(self, word):
        for w in self.words.keys():
            if(w == word):
                return True
        return False
    def process_response(self, text, bot_data):
        topweight = 0
        utext = text.upper()
        iwords = utext.lstrip().split(" ")
        results = []
        likable = 0
        if self.search_word(iwords[0]):
            for p in self.words[iwords[0]]:
                matches = p.regexp.findall(utext)
                if(len(matches) > 0):
                    if p.weight > topweight:
                        results.insert(len(results), ChatResult(p, p.weight, matches))
                        topweight = p.weight
                        likable = len(results)-1
        for p in self.words["*"]:
            matches = p.regexp.findall(utext)
            if(len(matches) > 0):
                if p.weight > topweight:
                    results.insert(len(results), ChatResult(p, p.weight, matches))
                    topweight = p.weight
                    likable = len(results)-1
        for p in self.words["_"]:
            matches = p.regexp.findall(utext)
            if(len(matches) > 0):
                if p.weight > topweight:
                    results.insert(len(results), ChatResult(p, p.weight, matches))
                    topweight = p.weight
                    likable = len(results)-1
        for p in self.words["?"]:
            matches = p.regexp.findall(utext)
            if(len(matches) > 0):
                if p.weight > topweight:
                    results.insert(len(results), ChatResult(p, p.weight, matches))
                    topweight = p.weight
                    likable = len(results)-1
        for p in self.words["^"]:
            matches = p.regexp.findall(utext)
            if(len(matches) > 0):
                if p.weight > topweight:
                    results.insert(len(results), ChatResult(p, p.weight, matches))
                    topweight = p.weight
                    likable = len(results)-1
        if(len(results) > 0):
            self.previous = results[likable].match.match
            if results[likable].match.conditional:
                if(results[likable].match.condition_meets(self.previous)):
                    return self.checkVars(results[likable].match.getreply(results[likable].matches, bot_data).lstrip())
            else:
                return self.checkVars(results[likable].match.getreply(results[likable].matches, bot_data).lstrip())
        else:
            return self.checkVars(self.default_pattern.getreply("", bot_data).lstrip())
    def checkVars(self, msg):
        setters = re.finditer(rep_params["setvar"], msg)
        getters = re.finditer(rep_params["getvar"], msg)
        result = msg

        for setter in setters:
            resx = re.sub(r'[,\.:]', "", setter.group(2))
            self.vars[setter.group(1)] = resx
            result = result.replace(setter.group(), resx)
        for getter in getters:
            result = result.replace(getter.group(), self.vars[getter.group(1)])
        return result
            

def preprocess(msg):
    operators = {
        " ":"\s",
        "*":"(.+)",
        "_":"(\\w+)",
        "?":"?(\\w+)?",
        "^":"?(.*)?"
    }
    temp = msg.lstrip().upper()
    for op in operators.keys():
        temp = temp.replace(op, operators[op])
    temp = re.sub("^\?+", "", temp)
    temp = "^" + temp.replace("\\s\\s", "\\s") + "$"
    return temp

# This class simbolizes a single pattern, which contains the pattern, it's weight, answers, and properties.
class ChatPattern():
    def __init__(self, match, reply, conditions=None):
        self.match = match
        self.regexp = re.compile(preprocess(match))
        self.weight = 1
        self.conditional = True if conditions!=None else False
        self.reverseconditional = False
        self.conditions = []
        if conditions != None:
            self.conditions = conditions
        if "*" in match or "_" in match or "*" in match or "?" in match:
            words = match.split(" ")
            temp = 0
            for w in words:
                if w in weights.keys():
                   self.weight = self.weight + weights[w]
                else:
                   self.weight = self.weight + 10

        self.replies = []
        for i in reply:
            self.replies.insert(len(self.replies), i.text)
    def getreply(self, matches, bot_data):
        if matches is None:
            if not self.conditional:
                return random.choice(self.replies)
        raw = random.choice(self.replies)
        params = rep_params["match_param"].findall(raw)
        bot_params = rep_params["match_bot_param"].findall(raw)
        for r in params:
            if type(matches[0]) is tuple:
                raw = raw.replace(r[0], matches[0][int(r[1])].lower())
            elif type(matches[0]) is str:
                raw = raw.replace(r[0], matches.lower())
            elif type(matches) is list:
                raw = raw.replace(r[0], matches[int(r[1])].lower())
        for b in bot_params:
            raw = raw.replace("$["+b+"]", bot_data[b])
        return raw
    def condition_meets(self, previous):
        if(self.conditions["type"]=="response"):
            if(self.conditions["value"]==previous):
                return True;
        return False;

# This class symbolizes a chat result. When the program looks for the most fitting answer
# to an input, it fills a list with these and selects the one with the biggest score (weight).
class ChatResult():
    def __init__(self,match,weight,matches):
        self.match = match
        self.weight = weight
        self.matches = matches
