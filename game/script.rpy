﻿#########################################
#       CHATBOT ENGINE IN RENPY         #
#########################################
#            By 3nigmatico              #
#                                       #
#    This is an implementation of my    #
#  chatbot engine in Python used in a   #
#   RenPy game for a more interactive   #
#              experience.              #
#                                       #
#########################################

# The script of the game goes in this file.

init python:
    import os
    # Import the chatbot module
    from chatbot import BotClient

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define e = Character("Sylvie")

# The game starts here.

label start:
# Initialize the chatbot engine using our python module
    python:
        bc = BotClient(renpy.file("chatbot/AI.json"))

        # Load the parameters from the chatbot folder. It must contain
        # an AI.json file and a BIOS.pyb file with all the data.
        # More 'pyb' files can be created in this folder with more
        # data.

        for filename in os.listdir(renpy.loader.transfn('chatbot')):
            if filename.endswith(".pyb"):
                bc.loadDict(renpy.file("chatbot/" + filename))

# Show a background. This uses a placeholder by default, but you can
# add a file (named either "bg room.png" or "bg room.jpg") to the
# images directory to show it.

    scene bg test

# This shows a character sprite. A placeholder is used, but you can
# replace it by adding a file named "eileen happy.png" to the images
# directory.

    show sylvie blue giggle

# Ask the user for text input, and then request
# the chatbot for an answer for that input.
    $chatline = ' '
    while chatline is not None:
        $ chatline = renpy.input("Say something")
        $ botresponse = bc.chat(chatline)
        e "[botresponse]"

    return
