# RenPy Chatbot Engine Concept

![demo](demo.gif)

## What is this?

This is a proof of concept for using a chatbot engine in the RenPy VN game engine.
It allows developers to use a chatbot engine in their games for a more interactive
experience!

The chatbot uses a modified version of the [Medea](https://gitlab.com/enigmatico/medea) chatbot engine.

## What do I need to use it?

Nothing! Just RenPy. It should work out of the box. You can even clone the project
directly into your RenPy folder and use it right out of the box if you want to
experiment with it by opening it with RenPy.

## How do I use it in my game?

All you need is to add the __chatbot.py__ file and the __chatbot__ folder inside of
your project directory. Then in your game script import the chatbot module and
initialize it using a python snippet, like this:

```python
init python:
    import os
    # Import the chatbot module
    from chatbot import BotClient

label start:
# Initialize the chatbot engine using our python module
    python:
        bc = BotClient(renpy.file("chatbot/AI.json"))

        # Load the parameters from the chatbot folder. It must contain
        # an AI.json file and a BIOS.pyb file with all the data.
        # More 'pyb' files can be created in this folder with more
        # data.

        for filename in os.listdir(renpy.loader.transfn('chatbot')):
            if filename.endswith(".pyb"):
                bc.loadDict(renpy.file("chatbot/" + filename))

```

Now all you need is to send your text input to the bot and ask for a reply using
the 'chat' function.
Example:

```python
# Ask the user for text input, and then request
# the chatbot for an answer for that input.
    $chatline = ' '
    while chatline is not None:
        $ chatline = renpy.input("Say something")
        $ botresponse = bc.chat(chatline)
        e "[botresponse]"
```

And that's pretty much it!

## How do I add more data into my bot?

The data for the chatbot is stored inside the __chatbot__ folder. By default there
are two files: chatbot.json (which contains the bot parameters) and BIOS.pyb.

You can edit the chatbot.json file to set the bot parameters or add new ones to
your liking. As for the BIOS.pyb file, it contains all the chat patterns for your
bot. This is the file you want to edit in order to add more data to your chatbot.

The chatbot uses a format kinda similar to AIML/SIML using an XML syntax. You
can also add additional pyb files if you don't want everything in the same
pyb file.

## Is it free to use

Yes! It is released under the GNU General Public License (GPL).